import SwiftUI

@main
struct FirebaseChatApp: App {
    var body: some Scene {
        WindowGroup {
            MainMessagesView()
        }
    }
}
